﻿using System;

namespace CMS.Data.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        CMSEntities Init();
    }
}