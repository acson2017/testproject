﻿using System.Data.Entity;

namespace CMS.Data
{
    public class CMSSeedData : DropCreateDatabaseIfModelChanges<CMSEntities>
    {
        protected override void Seed(CMSEntities context)
        {
            context.Commit();
        }
    }
}