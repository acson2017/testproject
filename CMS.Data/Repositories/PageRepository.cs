﻿using CMS.Data.Infrastructure;
using CMS.Models.Models;

namespace CMS.Data.Repositories
{
    public class PageRepository : RepositoryBase<Pages>, IPageRepository
    {
        public PageRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}