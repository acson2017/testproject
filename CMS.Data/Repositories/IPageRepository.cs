﻿using CMS.Data.Infrastructure;
using CMS.Models.Models;

namespace CMS.Data.Repositories
{
    public interface IPageRepository : IRepository<Pages>
    {
    }
}