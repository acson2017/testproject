﻿using CMS.Data.Infrastructure;
using CMS.Models.Models;

namespace CMS.Data.Repositories
{
    public interface IUserRepository : IRepository<Users>
    {
    }
}