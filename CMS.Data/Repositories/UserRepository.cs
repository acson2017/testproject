﻿using CMS.Data.Infrastructure;
using CMS.Models.Models;

namespace CMS.Data.Repositories
{
    public class UserRepository : RepositoryBase<Users>, IUserRepository
    {
        public UserRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}