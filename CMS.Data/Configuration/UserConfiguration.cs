﻿using CMS.Models.Models;
using System.Data.Entity.ModelConfiguration;

namespace CMS.Data.Configuration
{
    public class UserConfiguration : EntityTypeConfiguration<Users>
    {
        public UserConfiguration()
        {
            Property(u => u.UserName).IsRequired().HasMaxLength(100);
            Property(u => u.Email).IsRequired().HasMaxLength(200);
            Property(u => u.HashedPassword).IsRequired().HasMaxLength(200);
            Property(u => u.Salt).IsRequired().HasMaxLength(200);
            Property(u => u.IsLock).IsRequired();
        }
    }
}