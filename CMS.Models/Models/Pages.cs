﻿using System;

namespace CMS.Models.Models
{
    public class Pages
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int LanguageId { get; set; }

        public DateTime CreateDateTime
        {
            get; set;

        }
        public DateTime UpdateDate { get; set; }
    }
}