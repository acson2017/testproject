﻿using System.Collections.Generic;

namespace CMS.Models.Models
{
    public class Users : IEntityBase
    {
        public Users()
        {
            UserRoles=new  List<UserRole>();
        }
        public int ID { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
        public bool IsLock { get; set; }
        public string State { get; set; }
        public string HashedPassword { get; set; }
        public string Salt { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }

}