﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CMS.Models.Models;
using CMS.Service.Utilities;
namespace CMS.Service.Abstract
{
    public interface IMembershipService
    {

        CMS.Service.Utilities.MembeshipContext ValidateUser(string username, string password);
        Users CreateUser(string username, string email, string password, int[] roles);
        Users GetUser(int userId);
        List<Roles> GetUserRoles(string username);
    }
}
