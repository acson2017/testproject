﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using CMS.Models.Models;
using CMS.Service.Abstract;
using CMS.Data.Infrastructure;
using CMS.Data.UserExtension;
using CMS.Service.Utilities;
namespace CMS.Service
{
    public class MembershipService : IMembershipService
    {
        private readonly IRepository<Users> _userRepository;
        private readonly IRepository<Roles> _roleRepository;
        private readonly IRepository<UserRole> _userRoleRepository;
        private readonly IEncryptionService _encryptionService;
        private readonly IUnitOfWork _unitOfWork;

        public MembershipService(IRepository<Users> userRepository, IRepository<Roles> roleRepository, IRepository<UserRole> userRoleRepository, IEncryptionService encryptionService, IUnitOfWork unitOfWork)
        {
            this._userRepository = userRepository;
            this._roleRepository = roleRepository;
            this._userRoleRepository = userRoleRepository;
            this._encryptionService = encryptionService;
            this._unitOfWork = unitOfWork;
        }
        public Users CreateUser(string username, string email, string password, int[] roles)
        {
            var existingUser = _userRepository.GetSingleByUsername(username);

            if (existingUser != null)
            {
                throw new Exception("Username is already in use");
            }

            var passwordSalt = _encryptionService.CreateSalt();

            var user = new Users()
            {
                UserName = username,
                Salt = passwordSalt,
                Email = email,
                IsLock = false,
                HashedPassword = _encryptionService.EncryptPassword(password, passwordSalt)

            };

            _userRepository.Add(user);

            _unitOfWork.Commit();

            if (roles != null || roles.Length > 0)
            {
                foreach (var role in roles)
                {
                    addUserToRole(user, role);
                }
            }

            _unitOfWork.Commit();

            return user;
        }
        //this is login servic
        //using on controller   MembershipContext _userContext = _membershipService.ValidateUser(user.Username, user.Password);
        //if _userContext.User != null--->login oke!
        public CMS.Service.Utilities.MembeshipContext ValidateUser(string username, string password)
        {
            var membershipCtx = new CMS.Service.Utilities.MembeshipContext();

            var user = _userRepository.GetSingleByUsername(username);
            if (user != null && isUserValid(user, password))
            {
                var userRoles = GetUserRoles(user.UserName);
                membershipCtx.User = user;

                var identity = new GenericIdentity(user.UserName);
                membershipCtx.Principal = new GenericPrincipal(
                    identity,
                    userRoles.Select(x => x.Name).ToArray());
            }

            return membershipCtx;
        }
        private void addUserToRole(Users user, int roleId)
        {
            var role = _roleRepository.GetById(roleId);
            if (role == null)
                throw new ApplicationException("Role doesn't exist.");

            var userRole = new UserRole()
            {
                RoleId = role.ID,
                UserId = user.ID
            };
            _userRoleRepository.Add(userRole);
        }

        public Users GetUser(int userId)
        {
            return _userRepository.GetById(userId);
        }




        public List<Roles> GetUserRoles(string username)
        {
            List<Roles> _result = new List<Roles>();

            var existingUser = _userRepository.GetSingleByUsername(username);

            if (existingUser != null)
            {
                foreach (var userRole in existingUser.UserRoles)
                {
                    _result.Add(userRole.Roles);
                }
            }

            return _result.Distinct().ToList();
        }
        private bool isPasswordValid(Users user, string password)
        {
            return string.Equals(_encryptionService.EncryptPassword(password, user.Salt), user.HashedPassword);
        }

        private bool isUserValid(Users user, string password)
        {
            if (isPasswordValid(user, password))
            {
                return !user.IsLock;
            }

            return false;
        }
    }
}
