﻿using CMS.Models.Models;

namespace CMS.Service
{
    public interface IUserService
    {
        bool Login(Users user);
    }
}