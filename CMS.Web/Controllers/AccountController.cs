﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CMS.Data.Infrastructure;
using CMS.Service;
using CMS.Service.Abstract;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using CMS.Web.Models;
using CMS.Service.Utilities;

namespace CMS.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IMembershipService _membershipService;
        private readonly IUnitOfWork _unitOfWork;

        public AccountController(IMembershipService membershipService, IUnitOfWork unitOfWork)
        {
            this._membershipService = membershipService;
            this._unitOfWork = unitOfWork;
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                MembeshipContext _userContext = _membershipService.ValidateUser(model.Username,model.Password);
                if (_userContext.User!=null)
                {
                    RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("","Can not login with this account");
                }
            }

            return View("Login",model);

        }
    }
}